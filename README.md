# Personal computer Ansible setup script

An Ansible script to install and configure an Ubuntu based OS to my liking. Some modularity included.

## Prerequisites:

### Ubuntu 22.04

`sudo apt install -y software-properties-common python3-pip && python3 -m pip install ansible && sed -i '1s/^/export PATH=$HOME\/.local\/bin:$PATH\n\n/' ~/.bashrc && source ~/.bashrc`

## Usage:

Run the Ansible script:
`sudo echo "" && ANSIBLE_FORCE_COLOR=1 ansible-pull --url https://gitlab.com/_--_/personal-setup.git`

Parameters can be appended to that command with this syntax:
`--extra-vars "key1=value key2=value"`

### Some of the available boolean parameters and their default values:

```
m_core=true
m_power_user=false
m_games=false
m_hibernation=false
m_magic_sysrq=false
m_pidgin=false
m_windows_compatibility=false
m_pyautogui=false
m_touch_support=false
m_trackpad_gestures=false
m_japanese=false
m_dotfiles=false
m_my_kde=false
m_my_pc=false
m_job_mms=false
m_id_card: false

is_migrating=false
is_migrating_all=false
```

See more about these vars in `vars/default.yml`. The prefix `m_` stands for "module". A few modules depend on other modules (e.g. `games` also runs `windows-compatibility`).